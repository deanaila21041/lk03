/*
 * Jangan ubah kode didalam kelas Main ini
 * Lakukan modifikasi kode pada kelas pertambahan dan pengurangan
 * Dalam konteks kalkulator, pada kondisi apa interface akan lebih cocok digunakan daripada abstract class?
 * "Jawab disini..." 
 * Interface lebih cocok digunakan dalam konteks kalkulator ketika terdapat banyak jenis operasi matematika yang berbeda yang perlu diimplementasikan, misalnya penjumlahan, pengurangan, perkalian, dan pembagian. 
 * Dengan menggunakan interface, setiap operasi matematika dapat diimplementasikan dalam kelas yang berbeda-beda yang mengimplementasikan interface yang sama. 
 * Sehingga memungkinkan fleksibilitas yang lebih besar dalam mengubah dan menambahkan operasi matematika baru tanpa mempengaruhi kelas lain yang bergantung pada operasi matematika tersebut. 
 */

import java.util.*;

public class Main {   
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukan operan 1: ");
        double operan1 = scanner.nextDouble();

        System.out.print("Masukan operan 2: ");
        double operan2 = scanner.nextDouble();

        System.out.print("Masukan operator (+ atau -): ");
        String operator = scanner.next();
        scanner.close();

        Kalkulator kalkulator;
        if (operator.equals("+")) {
            kalkulator = new Pertambahan();
        } else if (operator.equals("-")) {
            kalkulator = new Pengurangan();
        } else {
            System.out.println("Operator tidak valid!");
            return;
        }

        double result = kalkulator.hitung(operan1, operan2);
        System.out.println("Result: " + result);
    }
}