class Pengurangan implements Kalkulator {
    @Override
    public double hitung(double operan1, double operan2) {
        // Menghitung pengurangan antara operan1 dan operan2, kemudian mengembalikan hasilnya sebagai nilai double
        return operan1 - operan2;
    }
}