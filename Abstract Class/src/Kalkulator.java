abstract class Kalkulator {
    double operan1, operan2;

    // Membuat method abstract setOperan untuk mengatur nilai operand-operand kalkulator, dengan parameter operand1 sebagai operan pertama dan operand2 sebagai operan kedua
    public abstract void setOperan(double operand1, double operand2);
    // Membuat method hitung() untuk melakukan perhitungan menggunakan operand-operand kalkulator, dan mengembalikan hasil perhitungan tersebut
    public abstract double hitung();
}