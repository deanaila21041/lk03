class Pengurangan extends Kalkulator {
    // Deklarasi variabel operan1 dan operan2
    double operan1,operan2;

    @Override
    public void setOperan(double operand1, double operand2) {
        // Mengatur nilai operan1
        this.operan1 = operand1;
        // Mengatur nilai operan2
        this.operan2 = operand2;
    }

    @Override
    public double hitung() {
        // Menghitung pengurangan antara operan1 dan operan2 dan mengembalikan hasilnya sebagai nilai double
        return operan1 - operan2;
    }
}