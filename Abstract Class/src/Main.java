/*
 * Jangan ubah kode didalam kelas Main ini
 * Lakukan modifikasi kode pada kelas kalkulator, pertambahan, dan pengurangan
 * Dalam konteks kalkulator, pada kondisi apa abstract class akan lebih cocok digunakan daripada interface?
 * "Jawab disini..." 
 * Abstract class akan lebih cocok digunakan daripada interface dalam beberapa kondisi berikut:
 * Dalam konteks kalkulator, abstract class akan lebih cocok digunakan daripada interface apabila terdapat metode-metode dengan implementasi yang umum dan dapat digunakan oleh kelas turunan, dan jika diperlukan adanya implementasi default untuk beberapa metode. 
 * Abstract class cocok juga digunakan apabila terdapat hubungan "is-a" yang kuat antara abstract class dan kelas turunannya, dan jika diperlukan penggunaan variabel anggota yang dapat digunakan oleh kelas-kelas turunan. 
 * Pada penugasan ini, abstract class digunakan sebagai parent class untuk kelas turunan Pengurangan dan Pertambahan yang membutuhkan implementasi metode setOperan() dan hitung(), sehingga abstract class lebih cocok digunakan daripada interface.
 */

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukan operan 1: ");
        double operand1 = scanner.nextDouble();

        System.out.print("Masukan operan 2: ");
        double operand2 = scanner.nextDouble();

        System.out.print("Masukan operator (+ atau -): ");
        String operator = scanner.next();
        scanner.close();

        Kalkulator kalkulator;
        if (operator.equals("+")) {
            kalkulator = new Pertambahan();
        } else if (operator.equals("-")) {
            kalkulator = new Pengurangan();
        } else {
            System.out.println("Operator tidak valid!");
            return;
        }

        kalkulator.setOperan(operand1, operand2);
        double result = kalkulator.hitung();
        System.out.println("Hasil: " + result);
    }
}